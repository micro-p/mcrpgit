set relativenumber
set cursorline
set smarttab
set hlsearch
set backspace=indent,eol,start
set undolevels=10000
set ignorecase
syntax on
